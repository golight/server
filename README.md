# Server

HTTP server of Golite ecosystem.

## Установка

```shell
go get "gitlab.com/golight/server"
```

## Доступные функции и методы

### Новый HTTP сервер

```go
//Функция, которая создает новый экземпляр сервера с заданными параметрами. Она принимает логгер и опциональные функции для настройки сервера.
    func NewHTTPServer(logger *zap.Logger, opts ...ServerOpt)
```

### Опциональные функции для настройки сервера

```go
// Функция позволяющую установить пользовательский объект сервера вместо стандартного.
    func WithHTTPServer(srv HTTPServerFace) ServerOpt{}
// Функция, позволяющую установить обработчик HTTP запросов для сервера.
    func WithHTTPHandler(h http.Handler) ServerOpt
// Функция, позволяющую установить конфигурацию для сервера.
    func WithConfig(conf config.Config) ServerOpt
// Метод Serve на Server запускает сервер и ожидает его завершения, обрабатывая ошибки и логируя события.
    func (s *Server) Serve(ctx context.Context) error
```

## Пример использования

```go
package main

import (
	"context"
	"gitlab.com/golight/server"
	"go.uber.org/zap"
	"golang.org/x/sync/errgroup"
	"net/http"
)

func main() {
	//Создается новый экземпляр логгера zap, который используется для записи логов в производственной среде.
	logger, err := zap.NewProduction()
	if err != nil {
		panic(err)
	}
	//Создается новый HTTP сервер с использованием пользовательского логгера и обработчика запросов, определенного функцией handler
	srv := server.NewHTTPServer(
		logger,
		server.WithHTTPHandler(
			http.HandlerFunc(handler),
		),
	)
	//Создается контекст с возможностью отмены операций, связанных с этим контекстом.
	ctx, cancel := context.WithCancel(context.Background())
	//Убеждаемся, что cancel будет вызван при завершении функции main, чтобы освободить ресурсы
	defer cancel()
	//Создается группа ошибок, которая позволяет запускать несколько горутин и агрегировать их ошибки.
	errGrp, ctx := errgroup.WithContext(ctx)
	//Запускаются два горутина в рамках группы ошибок. Первый слушает сигналы операционной системы для корректной обработки таких событий как SIGINT или SIGTERM, второй запускает HTTP сервер
	errGrp.Go(func() error {
		return server.HandleSignals(ctx, cancel)
	})

	errGrp.Go(func() error {
		return srv.Serve(ctx)
	})
	//Ожидается завершение всех горутин в группе ошибок. Если происходит ошибка, она записывается в лог
	if err = errGrp.Wait(); err != nil {
		logger.Error("server error", zap.Error(err))
	}
}

// Определяется обработчик HTTP запросов, который просто отправляет строку "Hello, world!" в ответ на любой запрос.
func handler(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Hello, world!!"))
}

```