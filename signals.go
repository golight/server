package server

import (
	"context"
	"os"
	"os/signal"
	"syscall"
)

func HandleSignals(ctx context.Context, cancel context.CancelFunc) error {
	s := make(chan os.Signal, 1)
	signal.Notify(s, os.Interrupt, syscall.SIGTERM)
	select {
	case <-s:
		cancel()
		return nil
	case <-ctx.Done():
		return ctx.Err()
	}
}
