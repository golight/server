package main

import (
	"context"
	"gitlab.com/golight/server"
	"go.uber.org/zap"
	"golang.org/x/sync/errgroup"
	"net/http"
)

func main() {
	logger, err := zap.NewProduction()
	if err != nil {
		panic(err)
	}
	srv := server.NewHTTPServer(
		logger,
		server.WithHTTPHandler(
			http.HandlerFunc(handler),
		),
	)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	errGrp, ctx := errgroup.WithContext(ctx)
	// os.Signal handling
	errGrp.Go(func() error {
		return server.HandleSignals(ctx, cancel)
	})

	errGrp.Go(func() error {
		return srv.Serve(ctx)
	})

	if err = errGrp.Wait(); err != nil {
		logger.Error("server error", zap.Error(err))
	}
}

// handlerfunc
func handler(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Hello, world!"))
}
