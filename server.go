package server

import (
	"context"
	"fmt"
	"go.uber.org/zap"
	"golang.org/x/sync/errgroup"
	"net/http"
	"time"
)

// Интерфейс, который определяет методы для запуска и остановки сервера.
type HTTPServerFace interface {
	ListenAndServe() error
	Shutdown(ctx context.Context) error
}

type Config struct {
	Address           string
	ShutdownTimeout   time.Duration
	Port              string
	ReadTimeout       time.Duration // Added for read timeout configuration
	WriteTimeout      time.Duration // Added for write timeout configuration
	ReadHeaderTimeout time.Duration // Added for read header timeout configuration
	IdleTimeout       time.Duration // Added for idle timeout configuration
}

// Интерфейс, который определяет метод Serve для запуска сервера с использованием контекста.
type ServerFace interface {
	Serve(ctx context.Context) error
}

// Структура, которая инкапсулирует логику работы сервера. Она содержит конфигурацию, логгер и объект, реализующий интерфейс HTTPServerFace.
type Server struct {
	conf   Config
	logger *zap.Logger
	srv    HTTPServerFace
}

// Это тип функции, которая принимает указатель на Server и изменяет его. Это позволяет использовать опциональный подход для настройки сервера.
type ServerOpt func(*Server)

// Функция, которая создает новый экземпляр сервера с заданными параметрами. Она принимает логгер и опциональные функции для настройки сервера.
func NewHTTPServer(logger *zap.Logger, opts ...ServerOpt) ServerFace {
	s := &Server{
		logger: logger,
		conf: Config{
			ShutdownTimeout:   5 * time.Second,
			Port:              ":8080",
			ReadTimeout:       1 * time.Second,  // Default value
			WriteTimeout:      1 * time.Second,  // Default value
			ReadHeaderTimeout: 2 * time.Second,  // Default value
			IdleTimeout:       60 * time.Second, // Default value
		},
		srv: &http.Server{
			// Apply configurations here
		},
	}

	for _, opt := range opts {
		opt(s)
	}

	if v, ok := s.srv.(*http.Server); ok {
		// Applying server configurations
		if s.conf.Port != "" {
			v.Addr = fmt.Sprintf("%s:%s", s.conf.Address, s.conf.Port)
		}
		if s.conf.ReadTimeout != 0 {
			v.ReadTimeout = s.conf.ReadTimeout
		}
		if s.conf.WriteTimeout != 0 {
			v.WriteTimeout = s.conf.WriteTimeout
		}
		if s.conf.IdleTimeout != 0 {
			v.IdleTimeout = s.conf.IdleTimeout
		}
		if s.conf.ReadHeaderTimeout != 0 {
			v.ReadHeaderTimeout = s.conf.ReadHeaderTimeout
		}
	}

	return s
}

// Функция, которая возвращает ServerOpt, позволяющую установить пользовательский объект HTTPServerFace вместо стандартного.
func WithHTTPServer(srv HTTPServerFace) ServerOpt {
	return func(s *Server) {
		s.srv = srv
	}
}

// Функция, которая возвращает ServerOpt, позволяющую установить обработчик HTTP запросов для сервера.
func WithHTTPHandler(h http.Handler) ServerOpt {
	return func(s *Server) {
		if v, ok := s.srv.(*http.Server); ok {
			v.Handler = h
		}
	}
}

// Функция, которая возвращает ServerOpt, позволяющую установить конфигурацию для сервера.
func WithConfig(conf Config) ServerOpt {
	return func(s *Server) {
		s.conf = conf
	}
}

// Метод Serve на Server запускает сервер и ожидает его завершения, обрабатывая ошибки и логируя события.
func (s *Server) Serve(ctx context.Context) error {
	var errGrp *errgroup.Group
	var grpCtx context.Context
	errGrp, grpCtx = errgroup.WithContext(ctx)

	errGrp.Go(func() error {
		s.logger.Info("server started", zap.String("port", s.conf.Port))
		if err := s.srv.ListenAndServe(); err != http.ErrServerClosed {
			s.logger.Error("http listen and serve error", zap.Error(err))
			return err
		}
		return nil
	})
	errGrp.Go(func() error {
		if err := errGrp.Wait(); err != nil {
			return err
		}
		return nil
	})

	ctxShutdown, cancel := context.WithTimeout(ctx, s.conf.ShutdownTimeout)
	defer cancel()
	select {
	case <-grpCtx.Done():
		s.logger.Info("server unexpectedly stopped", zap.Error(grpCtx.Err()))
		return grpCtx.Err()
	case <-ctx.Done():
		s.logger.Info("server stopping...")
	}

	if err := s.srv.Shutdown(ctxShutdown); err != nil {
		s.logger.Error("server shutdown error", zap.Error(err))
		return err
	}

	s.logger.Info("server stopped")

	return nil
}
