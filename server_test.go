package server

import (
	"context"
	"fmt"
	"github.com/stretchr/testify/assert"
	"go.uber.org/zap"
	"log"
	"net/http"
	"os"
	"os/signal"
	"reflect"
	"syscall"
	"testing"
	"time"
)

// MockHTTPServer имитирует HTTPServerFace для тестирования
type MockHTTPServer struct{}

func (m *MockHTTPServer) ListenAndServe() error {
	// Имитация работы сервера без реального запуска
	// Можно добавить time.Sleep для имитации задержки
	return http.ErrServerClosed
}

func (m *MockHTTPServer) Shutdown(ctx context.Context) error {
	// Имитация корректной остановки сервера
	return nil
}

func TestHTTPServerGracefulShutdown(t *testing.T) {
	logger, _ := zap.NewProduction()

	// Инициализация сервера с моковым HTTP сервером
	s := NewHTTPServer(logger, WithHTTPServer(&MockHTTPServer{})).(*Server)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Запуск сервера в отдельной горутине
	go func() {
		if err := s.Serve(ctx); err != nil {
			t.Errorf("Failed to serve: %v", err)
		}
	}()

	// Даем серверу немного времени для запуска
	time.Sleep(1 * time.Second)

	// Вызываем Shutdown для сервера
	ctxShutdown, cancelShutdown := context.WithTimeout(context.Background(), 1*time.Second)
	defer cancelShutdown()

	if err := s.srv.Shutdown(ctxShutdown); err != nil {
		t.Errorf("Server shutdown failed: %v", err)
	}
}

func TestNewHTTPServer(t *testing.T) {
	logger, _ := zap.NewProduction()
	type args struct {
		logger *zap.Logger
		opts   []ServerOpt
	}
	tests := []struct {
		name string
		args args
		want *Server
	}{
		{
			name: "ok",
			args: args{
				logger: logger,
				opts:   nil,
			},
			want: &Server{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := NewHTTPServer(tt.args.logger, tt.args.opts...)
			if got == nil {
				t.Fatal("NewHTTPServer() returned nil")
			}
		})
	}
}

func TestWithConfig(t *testing.T) {
	type args struct {
		conf Config
	}
	tests := []struct {
		name string
		args args
		want func(*Server) // Изменено на функцию, которая модифицирует Server
	}{
		{
			name: "ok",
			args: args{conf: Config{
				Address:           ":8080",
				ShutdownTimeout:   5 * time.Second,
				Port:              "8080",
				ReadTimeout:       30 * time.Second,
				WriteTimeout:      15 * time.Second,
				ReadHeaderTimeout: 10 * time.Second,
				IdleTimeout:       120 * time.Second,
			}},
			want: func(s *Server) { // Ожидаемая функция модификации Server
				assert.Equal(t, ":8080", s.conf.Address)
				assert.Equal(t, 5*time.Second, s.conf.ShutdownTimeout)
				assert.Equal(t, "8080", s.conf.Port)
				assert.Equal(t, 30*time.Second, s.conf.ReadTimeout)
				assert.Equal(t, 15*time.Second, s.conf.WriteTimeout)
				assert.Equal(t, 10*time.Second, s.conf.ReadHeaderTimeout)
				assert.Equal(t, 120*time.Second, s.conf.IdleTimeout)
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			opt := WithConfig(tt.args.conf)
			server := &Server{} // Создаем пустой сервер для тестирования
			opt(server)         // Применяем полученную опцию к серверу
			// Вызываем ожидаемую функцию для проверки, что сервер был правильно модифицирован
			tt.want(server)
		})
	}
}

func TestWithHTTPHandler(t *testing.T) {
	// Создаем пустой сервер для тестирования
	server := &Server{
		srv: &http.Server{},
	}

	// Создаем тестовый обработчик
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		write, err := w.Write([]byte("Test handler"))
		if err != nil {
			return
		}
		_ = write
	})

	// Применяем функцию WithHTTPHandler к серверу
	WithHTTPHandler(handler)(server)

	// Получаем интерфейс HTTPServerFace и приводим его к *http.Server
	if httpServer, ok := server.srv.(*http.Server); ok {
		// Проверяем, что обработчик был установлен правильно
		if httpServer.Handler == nil || reflect.ValueOf(httpServer.Handler).Pointer() != reflect.ValueOf(handler).Pointer() {
			t.Errorf("Ожидался обработчик %v, но установлен %v", handler, httpServer.Handler)
		}
	} else {
		t.Error("Не удалось привести HTTPServerFace к *http.Server")
	}
}

func TestWithHTTPServer(t *testing.T) {
	// Создаем тестовый HTTP сервер
	testHTTPServer := &http.Server{
		Addr: ":8080",
	}

	// Создаем пустой сервер для тестирования
	server := &Server{}

	// Применяем функцию WithHTTPServer к серверу
	WithHTTPServer(testHTTPServer)(server)

	// Проверяем, что srv был установлен правильно
	if server.srv != testHTTPServer {
		t.Errorf("Ожидался HTTP сервер %p, но установлен %p", testHTTPServer, server.srv)
	}
}

func TestServer_Serve(t *testing.T) {
	// Настройка логгера и конфигурации сервера
	logger, _ := zap.NewDevelopment()
	conf := Config{
		Port:            ":8080",
		ShutdownTimeout: time.Second * 5,
	}

	// Инициализация сервера с логгером и конфигурацией
	s := &Server{
		logger: logger,
		conf:   conf,
		srv:    &http.Server{Addr: conf.Port},
	}

	t.Run("server starts successfully", func(t *testing.T) {
		ctx := context.Background()
		done := make(chan bool)
		go func() {
			err := s.Serve(ctx)
			if err != nil {
				t.Errorf("expected no error, got %v", err)
			}
			done <- true
		}()
		time.Sleep(1 * time.Second)
		select {
		case <-done:
			t.Error("server took too long to start")
			return
		case <-time.After(time.Second * 1):
			log.Println("done!!!")
		}
	})

	// Добавьте больше тестов для различных сценариев
}

func TestHandleSignals(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	go func() {
		time.Sleep(1 * time.Second)
		pid := os.Getpid()

		// Отправляем сигнал SIGTERM другому процессу
		err := syscall.Kill(pid, syscall.SIGTERM)
		if err != nil {
			fmt.Printf("Ошибка при отправке сигнала: %v\n", err)
		}
	}()
	// Вызываем функцию, которую мы тестируем
	err := HandleSignals(ctx, cancel)

	// Проверяем, был ли вызван cancel
	if err != nil || reflect.ValueOf(cancel).Pointer() == 0 {
		t.Errorf("Expected cancel to be called, but was not")
	}

	// Переворачиваем обратно глобальный канал сигналов
	signal.Reset(os.Interrupt, syscall.SIGTERM)
}
func TestHandleSignals2(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	go func() {
		//time.Sleep(1 * time.Second)
		cancel()
	}()
	// Вызываем функцию, которую мы тестируем
	err := HandleSignals(ctx, cancel)

	// Проверяем, был ли вызван cancel
	if err == nil || reflect.ValueOf(cancel).Pointer() == 0 {
		t.Errorf("Expected cancel to be called, but was not")
	}

	// Переворачиваем обратно глобальный канал сигналов
	signal.Reset(os.Interrupt, syscall.SIGTERM)
}
